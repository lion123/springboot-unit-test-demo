package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.example.demo.SpringbootUnitTestDemoApplicationTests;
import com.example.demo.model.UserTest;
import com.example.demo.service.IUserTestService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

@AutoConfigureMockMvc
@Slf4j
public class ControllerTest extends SpringbootUnitTestDemoApplicationTests {

    private MockMvc mockMvc;
    @Mock
    private IUserTestService service;
    @InjectMocks
    private TestController testController;
    @Autowired
    private WebApplicationContext context;


    @Before
    public void setUp() throws Exception{
        log.info(">>>>>>>>>>setUp");
        MockitoAnnotations.initMocks(this);
        //MockMvcBuilders.webAppContextSetup(WebApplicationContext context)：指定WebApplicationContext，将会从该上下文获取相应的控制器并得到相应的MockMvc；
        //mockMvc = MockMvcBuilders.webAppContextSetup(context).build();//建议使用这种
        mockMvc = MockMvcBuilders.standaloneSetup(testController).build();
    }

    @After
    public void tearDown(){
    }

    @Test
    public void gitByIdTest1() throws Exception {
        UserTest  userTest = new UserTest("1", "zhangsan", "123456", new Date());
        Mockito.when(service.gitById("1")).thenReturn(userTest);

        mockMvc = MockMvcBuilders.standaloneSetup(testController).build();

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/p1/p11")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JSON.toJSONString(userTest)));
                //.andExpect(MockMvcResultMatchers.status().isOk());
        String result = resultActions.andReturn().getResponse().getContentAsString();

        log.info(">>>>>>>>>>{}",result);
    }

    @Test
    public void gitByIdTest2() throws Exception {
        UserTest  userTest = new UserTest("1", "zhangsan", "123456", new Date());
        Mockito.when(service.gitById("1")).thenReturn(userTest);

        mockMvc = MockMvcBuilders.standaloneSetup(testController).build();

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/p1/p12")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JSON.toJSONString(userTest)));
                //.andExpect(MockMvcResultMatchers.status().isOk());
        String result = resultActions.andReturn().getResponse().getContentAsString();

        log.info(">>>>>>>>>>{}",result);
    }

}
