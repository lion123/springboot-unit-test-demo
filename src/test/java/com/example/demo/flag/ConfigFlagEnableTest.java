package com.example.demo.flag;

import com.example.demo.SpringbootUnitTestDemoApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest(classes = SpringbootUnitTestDemoApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ConfigFlagEnableTest {

    @Value("${feature.config.flag}")
    private Boolean flag;

    @Test
    public void flagEnableTest(){
        log.info(">>>>>>>>>>{}", flag);
        Assert.assertTrue(flag);
    }

}
