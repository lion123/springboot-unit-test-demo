package com.example.demo.service;

import com.example.demo.SpringbootUnitTestDemoApplicationTests;
import com.example.demo.dao.UserTestDao;
import com.example.demo.model.UserTest;
import com.example.demo.service.impl.UserTestServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Date;

@Slf4j
public class ServiceTest extends SpringbootUnitTestDemoApplicationTests {

    @Mock
    private UserTestDao testDao;
    @InjectMocks
    private UserTestServiceImpl service = new UserTestServiceImpl();

    @Before
    public void setUp() throws Exception{
    }

    @Test
    public void gitById(){
        UserTest  userTest = new UserTest("1", "zhangsan", "123456", new Date());
        Mockito.when(testDao.gitById("1")).thenReturn(userTest);

        String id = "1";
        UserTest test = service.gitById(id);
        log.info(">>>>>>>>>>{}",test.toString());

        Assert.assertNotNull(test);
    }

}
