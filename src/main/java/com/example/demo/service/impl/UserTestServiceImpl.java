package com.example.demo.service.impl;

import com.example.demo.dao.UserTestDao;
import com.example.demo.model.UserTest;
import com.example.demo.service.IUserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserTestServiceImpl implements IUserTestService {

    @Autowired
    private UserTestDao testDao;


    @Override
    public UserTest gitById(String id) {
        UserTest userTest = testDao.gitById(id);
        return userTest;
    }


}
