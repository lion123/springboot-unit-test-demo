package com.example.demo.controller;

import com.example.demo.model.UserTest;
import com.example.demo.service.IUserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/p1")
public class TestController  {

    @Autowired
    private IUserTestService service;


    @GetMapping(value = "/p11")
    @ResponseBody
    public UserTest gitById1(@Valid @RequestBody UserTest userRequest) {
        UserTest userTest = service.gitById(userRequest.getId());
        return userTest;
    }

    @PostMapping(value = "/p12")
    @ResponseBody
    public UserTest gitById2(@Valid @RequestBody UserTest userRequest) {
        UserTest userTest = service.gitById(userRequest.getId());
        return userTest;
    }

}
